Creating a public repository for visual documents explaining Drupal processes, components, systems and other useful visual elements.

We are creating SVG images so that they can be versioned through Git, so that the text in the document can be searchable, and to make it more easy to translate the graphics into other languages.

Tools
Google Drawing as a tool. Google Drawing provides an collaborative tool for editing diagrams. Images can be exported to SVG.

Background
http://chicago2011.drupal.org/conference/bof/visual-multimedia-documentation-drupal
[@TODO Add list of other tools generated at BoF session]
[@TODO Add links to existing discussions about SVG for Drupal]

